<html>
  <head>
    <title>Mark Pruitt Demo</title>
    <!-- Local style settings -->
    <link rel="stylesheet" type="text/css" href="./css/style.css" media="screen" />

    <!-- Include the latest JQuery. -->
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>

    <!-- Include .js file with JQuery code. -->
    <script src="./js/functions.js" type="text/javascript"></script>
  </head>

  <body>
  <!-- Include the PHP functions so we can call them. -->
  <?php require './php/functions.php'; ?>
    <div id="main">
      <div id ="searchbox">
      <input type="text" id="search" onkeyup="filterData()" placeholder="Search ...">
      <a href="#" id="close">Close</a>
      </div>

      <div id="tables">
        <!-- Call the function that prepares and echos the tables. -->
        <?php load_data(); ?>
      </div>
    </div>
  </body>
</html>
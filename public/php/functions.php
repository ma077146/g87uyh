<?php
/**
 * @file
 * Functions required for the app.
 * @category Functions_For_Demo_App.
 * 
 * @author Mark Pruitt <mspruitt@windstream.net>
 */

/**
 * Loads data from a text file.
 *
 * @return void
 */
function load_data() {
    // Load data and convert to an array.
    $records = json_decode(file_get_contents('./data/data.json.txt'));

    // Set colors to highlight inititials.
    $colors = array("Orange", "Blue", "Green", "MediumSlateBlue ");

    // Initialize a counter.
    $i = 0;
    
    // Label for People section.
    echo '<div id="label">People</div>';

     // Start the people table structure.
     echo '<table id="people"><tbody class="list">';

    // Start rendering people info.
    foreach ($records as $record) {
        // First, create the initials.
        $initials = explode(' ', $record->person_name);

        // If there is a 3rd part of the array, we had an initial.
        if (isset($initials[2])) {
            $record->initials = mb_substr($initials[0], 0, 1) .
            mb_substr($initials[2], 0, 1);
        } else {
            $record->initials = mb_substr($initials[0], 0, 1) .
            mb_substr($initials[1], 0, 1); 
        }
        echo '<tr><td><span class="box" style="background-color:' . $colors[$i] .';">' . $record->initials . '</span>' .
        '</td><td>' . $record->person_name . '</td><td>' . $record->person_title .
        '</td><td>' . $record->organization_name . '</td><td>' . $record->location .
        '</td></tr>';

        // Manipulate the counter.
        if ($i == 3) {
            $i = 0;
        } else {
            $i++;
        }
    }

    // Finish people table.
    echo '</tbody></table>';

    // Label for Organizations section.
    echo '<div id="label">Organizations</div>';
    
    // Start the organization table structure.
    echo '<table id="organizations"><tbody class="list">';
    
    // Reset the counter.
    $i = 0;

    foreach ($records as $record) {
        echo '<tr><td><span class="box" style="background-color:' . $colors[$i] . ';">' . $record->initials . '</span>' .
        '</td><td>' . $record->organization_name . '</td><td class="filler">' .
        $record->person_name .  '</td><td class="filler">' . $record->person_title .
        '</td><td>' . $record->location . '</td></tr>';

        if ($i == 3) {
            $i = 0;
        } else {
            $i++;
        }
    }

    // Finish the organizations table.
    echo '</tbody></table>';

}

?>
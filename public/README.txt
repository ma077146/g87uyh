[1]  Running the app:
     I use a MacBook, so PHP is built in with the OS install.  I just cd into
     the directory, and type "php -S localhost:8888 -t public/", then open
     a browser to localhost:8888 to see the program.

[2]  Apologies in advance for the rough front end code.  I have primarily been
     a backend developer; I had to figure out some of this as I went along.  I
     can still learn alot.

[3]  I used PHP for most of the heavy lifting, since you can more easily load files
     (he data.json.txt file) than in plain JavaScript.  You can also intermix HTML/PHP so
     it seemed like a good choice.

[4]  The color on the the initials was throwing me.  I can see it correlates the same
     record in the two tables.  Would love to know how you all really did it.  I'm sure
     it's more dynamic than what I did.

[5]  Given that the JSON provided ID numbers for the organizations and people, in a real world
     application those would likely be added to a url to make them clickable.  I bypassed that
     here since the end result seemed to be to closely mimic the display and search functionality.

[6]  The description of the task from Karyn seems different to me than the image.  The description
     said to put People in one table, Organizations in the other.  The image shows both in the localhost  
     table.  I went with the description.  If that was wrong, and you'd like to clarify, I'd be glad to
     take another try at it.

[7]  If I don't get any further than submitting this code in the interview process, "Thank you!".  
     I'm sure you had some good response to your job listing, and I wish you the best in finding
     the right person to add to your team.

